//Api Base url
var apiHost = "https://api.kcdev.pro/v2/";
// Authentication 
function AuthLogin() {
    var param = {
        username: $('#userName').val(),
        password: $('#password').val(),
        usertype: $('#usertype').val(),
        _extend: $('#_extend').val()
    };
	
	// remember Me settings
	rememberMe(param);
	 
	 // Login Api 
    api('auth', param, 'POST', function(err, data) {
		// if login success
        if (!err) {
            saveCredentials(data.response);
            location.href = "list.html";
        } else { // if login fails
            deleteCredentials();
            if (err.status === 401) {
                $("#errorMsg").append(err.responseJSON.response.fail_reason.replace('(1).', ". " + err.responseJSON.response.message))
            } else {
                location.href = "login.html";
            }
        }
    });
}


// Get all users with respect to account_id, start page and Authorize token
function getUsers(_start) {
	// checking localstorage to validate api with userdetais and token
    if (localStorage.getItem('userData') != null) {
        var userDetail = JSON.parse(localStorage.getItem('userData'));
        var endPoint = "accounts/" + userDetail.user.account_id + "/students?_start=" + _start + "&_limit=5&token=" + userDetail.token;
        api(endPoint, null, 'GET', function(err, data) {
			// if token expired or any issue in response
            if (err) {
                if (err.status === 498) {
                    alert(err.statusText)
                }
                location.href = "login.html";
                return false;
            }
            // called when api gets success
            pagination(data.pagination);
			// clearing old user list data
            $("#userList").empty();
			
			// creating new users data
            $.each(data.response, function(index, items) {

                var li = "<li><div class='row user-row'><div class='col-md-2 col-sm-2 col-xs-2 text-right'>" +
                    "<i class='fa fa-check-circle userchecked' aria-hidden='true'></i>" +
                    "</div>" +
                    "<div class='col-md-6 col-sm-6 col-xs-6'>" +
                    "    <p class='user-name'>" + items.username + "</p>" +
                    "    <p class='user-data'>" + items.first_name + " " + items.last_name + "</p>" +
                    "</div>" +
                    "<div class='col-md-4 col-sm-4 col-xs-4'>" +
                    "        <i class='fa fa-ellipsis-h more-detail' aria-hidden='true'></i>" +
                    "        <p class='mb-0'>" + items.group + "</p>" +
                    "</div>" +
                    "</div>" +
                    " </li>";

				// Append each user in list
                $("#userList").append(li);
            });
        })
    } else {
        location.href = "login.html";
    }
}

// pagination for user list
function pagination(data) {
    $("#userPaging").empty();

    var li = "";

    for (index = 0; index < data.totalPages; index++) {
        var isActive = data.start == index ? 'active' : '';

        if (isActive != "") {

            li = li + "<li><a href='#' class=" + isActive + ">" + Number(index + 1) + "</a></li>";
        } else {

            li = li + "<li><a href='#' onClick='getUsers(" + index + ")'>" + Number(index + 1) + "</a></li>";
        }

    }
    if (data.start < data.totalPages - 1) {
        li = li + "<li><a href='#' onClick='getUsers(" + Number(data.start + 1) + ")'>Next <i class='fa fa-angle-double-right' aria-hidden='true'></i></a></li>";
    } else {
        li = li + "<li><a href='#' >Next <i class='fa fa-angle-double-right' aria-hidden='true'></i></a></li>";
    }

    $("#userPaging").append(li);

}

function logout() {

    if (localStorage.getItem('userData') != null) {
        var userDetail = JSON.parse(localStorage.getItem('userData'));
        var param = {
            token: userDetail.token,
            _method: "delete"
        };
        api('auth', param, 'POST', function(err, data) {
            if (!err) {
                deleteCredentials();
                location.href = "login.html";

            } else {
                deleteCredentials();
                location.href = "login.html";
            }
        });

    }
}

function saveCredentials(data) {
    localStorage.setItem('userData', JSON.stringify(data));
}

function deleteCredentials() {
    localStorage.removeItem('userData');
}

function rememberMe(param){
	
 if ($('#remember_me').is(':checked')) {
			// save username and password
			localStorage.usrname = param.username;
			localStorage.pass = param.password;
			localStorage.chkbx = $('#remember_me').val();
		} else {
			localStorage.usrname = '';
			localStorage.pass = '';
			localStorage.chkbx = '';
		}	
	
}

// url end point
// params if any or null
// verb GET, PUT, POST 
// Callback function , promise can be used 

function api(url, params, verb, callback) {

    $.ajax({
        url: apiHost + url,
        type: verb,
        dataType: 'JSON',
        data: params,
        success: function(data) {
            if (data && data.code === 200)
                callback(null, data)
            else
                callback("err")
        },
        error: function(err) {
            callback(err);
        },
    });

}